#include <stdio.h>
int sum(int x)
{
    int temp,sum=0;
    while(x>0)
    {
        temp=x%10;
        sum=sum+temp;
        x=x/10;
    }
    return sum;
}
int main()
{
    int n,ans;
    printf("enter the number\n");
    scanf("%d",&n);
    ans=sum(n);
    printf("the sum of digits of %d is %d",n,ans);
    return 0;
}